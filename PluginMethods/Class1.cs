﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTTPLib;
using System.IO;
using System.Web;
using System.Diagnostics;


namespace PluginMethods {
    public class GET : HTTPMethodHandler {

        public override string MethodName {
            get {
                return "GET";
            }
        }

        public override HTTPResponse GetResponse(HTTPRequest hrRequest, Dictionary<string, string> dictServerConfig) {
            string strStLn = "HTTP/1.1 200 OK";
            string strPath = dictServerConfig["DocumentRoot"] + "/" + HttpUtility.UrlDecode(hrRequest.URI);
            if (File.Exists(strPath)) {
                Process prProgram = new Process();
                prProgram.StartInfo.FileName = strPath;
                prProgram.Start();
                FileStream fsFile = File.Open(strPath, FileMode.Open, FileAccess.Read);
                List<string> lstHeaders = new List<string>();
                lstHeaders.Add("Connection: close");
                lstHeaders.Add("Content-Length: " + fsFile.Length.ToString());
                Dictionary<string, string> dictMIME = new Dictionary<string, string>();
                dictMIME.Add(".htm", "text/html");
                dictMIME.Add(".html", "text/html");
                dictMIME.Add(".css", "text/css");
                dictMIME.Add(".js", "text/javascript");
                dictMIME.Add(".gif", "image/gif");
                dictMIME.Add(".jpg", "image/jpeg");
                dictMIME.Add(".jpeg", "image/jpeg");
                dictMIME.Add(".png", "image/png");
                string strExtension = Path.GetExtension(strPath).ToLower();
                if (dictMIME.ContainsKey(strExtension)) {
                    lstHeaders.Add("Content-Type: " + dictMIME[strExtension]);
                } else {
                    lstHeaders.Add("Content-Type: text/plain");
                }
                lstHeaders.Add("Date: " + DateTime.Now.ToString("r"));
                lstHeaders.Add("Server: " + dictServerConfig["ServerName"]);
                return new HTTPResponse(strStLn, lstHeaders, fsFile);
            } else {
                string strStatusLine = "HTTP/1.1 404 Not Found";
                List<string> lstEmpty = new List<string>();
                return new HTTPResponse(strStatusLine, lstEmpty, null);
            }

        }
    }

    public class OPTIONS : HTTPMethodHandler {
        public override string MethodName {
            get {
                return "OPTIONS";
            }
        }

        public override HTTPResponse GetResponse(HTTPRequest hrRequest, Dictionary<string, string> dictServerConfig) {
            string strStLn = "HTTP/1.1 200 OK";
            List<string> lstHeaders = new List<string>();
            lstHeaders.Add("Date: " + DateTime.Now.ToString("r"));
            lstHeaders.Add("Server: " + dictServerConfig["ServerName"]);
            lstHeaders.Add("Allow: GET OPTIONS");
            return new HTTPResponse(strStLn, lstHeaders, null);
        }
    }
}
